package com.online.vendor.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "VENDORS")
public class Vendor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vendor_id")
	private Integer vendorId;
	@NotEmpty(message = "Item Name Is Mandatory")
	private String vendorName;
	@ManyToMany(mappedBy = "vendors", cascade = CascadeType.ALL)
	private List<Item> items;

	public Vendor() {
		super();
	}

	public Vendor(Integer vendorId, String vendorName, List<Item> items) {
		super();
		this.vendorId = vendorId;
		this.vendorName = vendorName;
		this.items = items;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

}
