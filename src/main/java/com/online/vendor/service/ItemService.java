package com.online.vendor.service;

import java.util.List;

/**
 * 
 * @author janbee
 *
 */
public interface ItemService {

	/**
	 * 
	 * @param itemIdList
	 * @return boolean
	 */
	public boolean checkAvailabilityOfItems(List<Integer> itemIdList);
}
