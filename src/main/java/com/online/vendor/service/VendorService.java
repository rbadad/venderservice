package com.online.vendor.service;

public interface VendorService {

	/**
	 * 
	 * @param vendorId
	 * @return boolean
	 */
	public boolean checkAvailabilityOfVendor(Integer vendorId);
}
