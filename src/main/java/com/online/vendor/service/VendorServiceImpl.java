package com.online.vendor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.vendor.repository.VendorRepository;

/**
 * 
 * @author janbee
 *
 */
@Service
public class VendorServiceImpl implements VendorService{

	@Autowired
	VendorRepository vendorRepository;
	
	@Override
	public boolean checkAvailabilityOfVendor(Integer vendorId) {
		return vendorRepository.existsById(vendorId);
	}

}
