package com.online.vendor.controller;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.online.vendor.dto.ItemResponseDTO;
import com.online.vendor.dto.VendorResponseDTO;
import com.online.vendor.service.CommonServiceImpl;
import com.online.vendor.service.VendorService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author prabirkumar.jena
 *
 */
@RestController
@RequestMapping
public class VendorController {
	private static final Logger LOGGER = LoggerFactory.getLogger(VendorController.class);
	@Autowired
	VendorService vendorService;
	
	/**
	 * 
	 * @param vendorId
	 * @return boolean
	 */
	@GetMapping("/vendors/{vendorId}")
	public ResponseEntity<Boolean> checkAvailabilityOfVendor(@PathVariable Integer vendorId) {
		LOGGER.debug("VendorController :: checkAvailabilityOfVendor :: start");
		boolean isAvailable = vendorService.checkAvailabilityOfVendor(vendorId);
		LOGGER.debug("VendorController :: checkAvailabilityOfVendor :: end");
		return new ResponseEntity<>(isAvailable,HttpStatus.OK);
	}

	@Autowired
	CommonServiceImpl commonServiceImpl;

	
/**
 * 
 * @param name
 * @return resposneDto
 */
	@GetMapping("/vendors")
	@ApiOperation("Vendor Details By Vendor Name")
	@ApiResponses({ @ApiResponse(code = 4000, message = "Vendor details name not found"),
			@ApiResponse(code = 4001, message = "Item details not found") })
	public ResponseEntity<List<ItemResponseDTO>> getVendorsByVendorName(@NotEmpty(message = "Vendor Name Is Mandatory") @RequestParam(name = "name") String name) {
		LOGGER.debug("VendorController getVendorsByVendorName start");
		List<ItemResponseDTO> resposneDto = commonServiceImpl.getVendorsByVendorName(name);
		LOGGER.debug("VendorController getVendorsByVendorName end");
		return new ResponseEntity<>(resposneDto, HttpStatus.OK);
	}
/**
 * 
 * @param name
 * @return resposneDto
 */
	@GetMapping("/items")
	@ApiOperation("Item Details By Item Name")
	@ApiResponses({ @ApiResponse(code = 4000, message = "Vendor name not found"),
			@ApiResponse(code = 4001, message = "Item name not found") })
	public ResponseEntity<List<VendorResponseDTO>> getItemsByItemName(@NotEmpty(message = "Item Name Is Mandatory") @RequestParam(name = "name") String name) {
		LOGGER.debug("VendorController getItemsByItemName start");
		List<VendorResponseDTO> resposneDto = commonServiceImpl.getItemsByItemName(name);
		LOGGER.debug("VendorController getItemsByItemName end");
		return new ResponseEntity<>(resposneDto, HttpStatus.OK);
	}

}
