package com.online.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.vendor.entity.Item;

/**
 * 
 * @author janbee
 *
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

	Item findByItemName(String itemName);
}
