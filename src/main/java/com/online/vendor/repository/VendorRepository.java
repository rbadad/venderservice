package com.online.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.vendor.entity.Vendor;

/**
 * 
 * @author janbee
 *
 */
@Repository
public interface VendorRepository extends JpaRepository<Vendor, Integer>{

	Vendor findByVendorName(String vendorName);

}
