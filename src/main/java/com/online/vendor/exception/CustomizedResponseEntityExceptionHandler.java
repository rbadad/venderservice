package com.online.vendor.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.online.vendor.constants.AppConstants;

/**
 * @author prabirkumar.jena
 *
 */
@RestControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = VendorNameNotFoundException.class)
	public ResponseEntity<String> handleVendorNameNotFoundException() {
		return new ResponseEntity<>(AppConstants.VENDOR_NAME_NOT_FOUND, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = ItemNameNotFoundException.class)
	public ResponseEntity<String> handleItemNameNotFoundException() {
		return new ResponseEntity<>(AppConstants.ITEM_NAME_NOT_FOUND, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ErrorResponse errorDetails = new ErrorResponse("Validation Failed", details);

		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
}