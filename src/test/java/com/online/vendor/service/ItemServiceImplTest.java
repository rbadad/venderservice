package com.online.vendor.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.online.vendor.entity.Item;
import com.online.vendor.exception.ItemNotFoundException;
import com.online.vendor.repository.ItemRepository;

class ItemServiceImplTest {

	@InjectMocks
	ItemServiceImpl itemServiceImpl;

	@Mock
	ItemRepository itemRepository;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCheckAvailabilityOfItems() {
		Item item = new Item();
		List<Item> items = new ArrayList<>();
		List<Integer> itemss = new ArrayList<>();
		itemss.add(101);
		itemss.add(102);
		item.setItemId(101);
		item.setItemName("Chicken kabab");
		item.setItemPrice(150);
		Item item2 = new Item();
		item2.setItemId(102);
		item2.setItemName("Chicken masala");
		item2.setItemPrice(180);
		items.add(item);
		items.add(item2);

		when(itemRepository.findAllById(itemss)).thenReturn(items);
		boolean value = itemServiceImpl.checkAvailabilityOfItems(itemss);
		assertEquals(true, value);

	}

	@Test
	public void testGetImtemDetailsByItemId() {
		Item item = new Item();
		item.setItemId(101);
		item.setItemName("Chicken kabab");
		item.setItemPrice(150);
		when(itemRepository.findById(101)).thenReturn(Optional.of(item));
		Item item1 = itemServiceImpl.getImtemDetailsByItemId(101);
		assertEquals(101, item1.getItemId());
		assertEquals("Chicken kabab", item1.getItemName());
		assertEquals(150, item1.getItemPrice());
	}

	@Test
	public void testGetImtemDetailsByItemIdFailure() {
		Item item = new Item();
		item.setItemId(101);
		item.setItemName("Chicken kabab");
		item.setItemPrice(150);
		when(itemRepository.findById(101)).thenReturn(Optional.of(new Item()));
		try {
			itemServiceImpl.getImtemDetailsByItemId(101);
		} catch (ItemNotFoundException e) {
			assertEquals("Item not Found", e.getMessage());
		}

	}

}
